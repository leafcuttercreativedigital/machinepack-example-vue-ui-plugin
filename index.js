import MachinepackPayment from './MachinepackPayment'
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios'

const VuePlugin = {
	install: function (Vue, options) {
		if (!options || !options.apiRoot) {
			throw new 'Please set apiRoot in plugin options'
		}

		const api = axios.create({
			baseURL: options.apiRoot
		});

		Vue.prototype.$machinePack = {
			options: Object.assign({}, options),
			send: function (evt, data) {
				return api.post('/send', {
					event: evt,
					data: data
				})
			}
		}

		Vue.use(BootstrapVue);

		Vue.component(MachinepackPayment.name, MachinepackPayment)
	}
}

if (typeof window !== 'undefined' && window.Vue) {
	window.Vue.use(BootstrapVue);
	window.Vue.use(VuePlugin)
}

export default VuePlugin
