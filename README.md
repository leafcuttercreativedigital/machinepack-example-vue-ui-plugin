# MachinePack components

This package has several vue components for reuse. All components assume there
is a machinepack API server available via settings:

```
import Vue from 'vue'
import MachinePack from 'machinepack-vue-ui-plugin'

Vue.use(MachinePack, {
	apiRoot: 'YOUR_MACHINE_PACK_SERVER_GOES_HERE'
})

new Vue({
	el: '#machinepack-wordpress-plugin-app',
	render: h => h (App)
})
```

Once you load all components, it's recommended that you load bootstrap styles as well,
but only the ones you use, for example:

```
@import 'node_modules/bootstrap/scss/bootstrap-reboot.scss';
@import 'node_modules/bootstrap/scss/forms.scss';
@import 'node_modules/bootstrap/scss/nav.scss';
@import 'node_modules/bootstrap/scss/grid.scss';
@import 'node_modules/bootstrap/scss/buttons.scss';
@import 'node_modules/bootstrap/scss/alert.scss';
@import 'node_modules/bootstrap-vue/dist/bootstrap-vue.css';
```

Make sure you import boostrap-vue styles as well after all boostrap is loaded.
